import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
  public static void main(String[] args) {
    var logger = LoggerFactory.getLogger("testy");

    logger.debug("The Pups!");
  }
}
